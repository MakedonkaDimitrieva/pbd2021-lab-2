package si.uni_lj.fri.pbd.lab2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class ProfileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        val intent = intent as Intent
        val fullName = intent.getStringExtra(RegistrationActivity.EXTRA_NAME) as String

        val tv = findViewById<TextView>(R.id.textView)
        tv.text = fullName

        val msgButton = findViewById<Button>(R.id.button)

        msgButton.setOnClickListener {
            val context = applicationContext
            val editMsg = findViewById<EditText>(R.id.editTextTextPersonName)
            val msg = editMsg.text.toString()
            val duration = Toast.LENGTH_LONG
            val toast = Toast.makeText(context, msg, duration)
            toast.show()
        }
    }
}