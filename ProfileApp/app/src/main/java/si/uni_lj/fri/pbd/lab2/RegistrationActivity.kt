package si.uni_lj.fri.pbd.lab2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText

class RegistrationActivity : AppCompatActivity() {
    companion object {
        const val EXTRA_NAME = "si.uni-lj.fri.pbd.lab2.FULL_NAME"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
    }
    fun registerUser(view: View) {
        val nameText = findViewById<EditText>(R.id.edit_name)
        val fullName = nameText.text.toString()
        if (fullName.length < 0) {
            nameText.error = "Error!"
        }
        val intent = Intent(applicationContext, ProfileActivity::class.java)
        intent.putExtra(EXTRA_NAME, fullName)
        startActivity(intent)
    }
}